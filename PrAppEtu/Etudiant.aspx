﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Etudiant.aspx.cs" Inherits="PrAppEtu.Etudiant" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <h1>ENSA Khouribga</h1>
    <br />
    <h2>Liste des etudiants par classe</h2>
    <div>
        <h3>Classe:</h3>
        <p>
            <asp:FormView ID="FVClasse" runat="server" AllowPaging="True" DataKeyNames="Code" DataSourceID="sqlDSClasse" Width="110px">
                <EditItemTemplate>
                    Code:
                    <asp:Label ID="CodeLabel1" runat="server" Text='<%# Eval("Code") %>' />
                    <br />
                    Nom:
                    <asp:TextBox ID="NomTextBox" runat="server" Text='<%# Bind("Nom") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    Code:
                    <asp:TextBox ID="CodeTextBox" runat="server" Text='<%# Bind("Code") %>' />
                    <br />
                    Nom:
                    <asp:TextBox ID="NomTextBox" runat="server" Text='<%# Bind("Nom") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Code:
                    <asp:Label ID="CodeLabel" runat="server" Text='<%# Eval("Code") %>' />
                    <br />
                    Nom:
                    <asp:Label ID="NomLabel" runat="server" Text='<%# Bind("Nom") %>' />
                    <br />

                </ItemTemplate>
            </asp:FormView>
            <asp:SqlDataSource ID="sqlDSClasse" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Classe]"></asp:SqlDataSource>
        </p>
        <asp:SqlDataSource ID="SqlDEtudiants" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Etudiant] WHERE ([Classe] = @Classe)">
            <SelectParameters>
                <asp:QueryStringParameter Name="Classe" QueryStringField="Code" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:Repeater ID="RPTEtudiants" runat="server" DataSourceID="SqlDSJoin" OnItemCommand="RPTEtudiants_ItemCommand">
            <HeaderTemplate>
                <table border="1">
                    <tr>
                        <th>CIN</th>
                        <th>Nom</th>
                        <th>Photo</th>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <table border="1">
                    <tr>
                        <td>
                            <asp:Label ID="rpt" runat="server" Text='<% # Eval("CIN") %>' ></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<% # Eval("Nom") %>' ></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<% # Eval("Photo") %>' ></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:Repeater>
        <asp:SqlDataSource ID="SqlDSJoin" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Etudiant] WHERE ([Classe] = @Classe)">
            <SelectParameters>
                <asp:ControlParameter ControlID="FVClasse" Name="Classe" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
