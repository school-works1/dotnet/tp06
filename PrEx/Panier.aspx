﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Panier.aspx.cs" Inherits="PrEx.Panier" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TP 6</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="IdPanier" DataSourceID="SqlDSPanier">
                <Columns>
                    <asp:BoundField DataField="IdPanier" HeaderText="IdPanier" SortExpression="IdPanier" />
                    <asp:BoundField DataField="Couleur" HeaderText="Couleur" ReadOnly="True" SortExpression="Couleur" />
                    <asp:BoundField DataField="Taille" HeaderText="Taille" SortExpression="Taille" />
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                             <asp:LinkButton ID="ButtonSelect" runat="server" Text="Selectionner" onclientclick=''>
                             </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>  
                </Columns>
            </asp:GridView>
            <asp:DataList ID="Legume" runat="server" DataSourceID="SqlDSLegume" RepeatDirection="Horizontal" RepeatColumns="3">  
        <ItemTemplate>
            <table>
                <tr>
                    <td>Libelle</td>
                    <td> <%# Eval("Libelle") %></td>
                </tr>
                <tr>
                    <td></td>
                    <td><asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Image") %>' Height="180px" Width="200px" /></td>
                </tr>
                <tr>
                    <td>Prix</td>
                    <td> <%# Eval("Prix") %></td>
                </tr>
                <tr>
                    <td>Quantite</td>
                    <td> <%# Eval("Quantite") %></td>
                </tr>
            </table>
        </ItemTemplate>  
            </asp:DataList>  
        </div>
        <asp:SqlDataSource ID="SqlDSLegume" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="select Legume.Libelle as Libelle, Legume.Image as Image, Legume.Prix as Prix, Panier_Legume.Quantite as Quantite FROM Legume, Panier_Legume WHERE Legume.IdLegume=Panier_Legume.IdLegume AND Panier_Legume.IdPanier=300">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDSPanier" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Panier]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDSPanierLegume" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Panier_Legume]"></asp:SqlDataSource>

    </form>
</body>
</html>

